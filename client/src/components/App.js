import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Index from './Index';
import Header from './layouts/header/Index';
import Footer from './layouts/Footer/Footer'
import Register from './Register';
import NotFound from './NotFound';
import Contact from './Contact';
import axios from 'axios';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rcount: '',
			ccount: '',
		};
	}
	componentWillMount() {
		var self = this;
		axios.get('/backend/visitors').then(function (res) {
			self.setState({
				visitors:
					res.data['result'] === 'number'
						? parseInt(res.data['result'])
						: res.data['result']
			});
		});
		var self = this;
		axios.get('/backend/regcount').then(function (res) {
			self.setState({
				rcount:
					res.data['result'] === 'number'
						? parseInt(res.data['result'])
						: res.data['result'],
				ccount:
					res.data['result1'] === 'number'
						? parseInt(res.data['result1'])
						: res.data['result1'],
				visitors: res.data['visitors'] === 'number'
					? parseInt(res.data['visitors'])
					: res.data['visitors']
			});
		});
	}
	render() {
		return (
			<div id="lgx-home" className="lgx-container">
				<Header />
					<Switch>
					<Route path="/" component={Index} exact />
					<Route path="/register" component={Register} exact />
					<Route path="/contact" component={Contact} exact />
					<Route path="*" component={NotFound} exact />
					</Switch>
				<Footer />
			</div>
		);
	}
}
export default App;
