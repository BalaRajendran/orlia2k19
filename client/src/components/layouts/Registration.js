import React, { Component } from 'react';
import axios from 'axios';
var $ = require('jquery');

class Registration extends Component {
	constructor(props) {
		super(props);
		this.onChangeEvent = this.onChangeEvent.bind(this);
		this.onChangeCount = this.onChangeCount.bind(this);
		this.onChangeTeamname = this.onChangeTeamname.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
			event_name: '',
			mincount: '',
			maxcount: '',
			count: '',
			InputCount: '',
			name: {},
			roll: {},
			nameerror: '',
			rollerror: '',
			eventerror: '',
			counterror: '',
			teamname: '',
			teamnameerror: '',
			message: "",
			time: "on"
		};
	}
	onChangeTeamname(e) {
		this.setState({
			teamname: e.target.value,
			nameerror: '',
			rollerror: '',
			eventerror: '',
			counterror: '',
			teamnameerror: '',
			message: ""
		});
	}
	onChangeEvent(e) {
		this.setState({
			InputCount: '',
			nameerror: '',
			rollerror: '',
			eventerror: '',
			counterror: '',
			teamnameerror: '',
		});
		if (this.state.time == "off") {
			if (e.target.value == 'Rock n roll') {
				this.setState({
					event_name: 'Rock n roll',
					mincount: 8,
					maxcount: 31,
					count: 5,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'இரட்டைக் ௧திரே') {
				this.setState({
					event_name: 'இரட்டைக் ௧திரே',
					mincount: 2,
					maxcount: 3,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'சித்திரம் பேசுதடி') {
				this.setState({
					event_name: 'சித்திரம் பேசுதடி',
					mincount: 1,
					maxcount: 2,
					count: 1,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'கண்ணாமூச்சி ரே.. ரே..') {
				this.setState({
					event_name: 'கண்ணாமூச்சி ரே.. ரே..',
					mincount: 2,
					maxcount: 3,
					count: 1,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'நெருப்பின்றி புகையாது') {
				this.setState({
					event_name: 'நெருப்பின்றி புகையாது',
					mincount: 1,
					maxcount: 3,
					count: 1,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'சிரிச்சா போச்சு [Comedy]') {
				this.setState({
					event_name: 'சிரிச்சா போச்சு [Comedy]',
					mincount: 2,
					maxcount: 6,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'சிரிச்சா போச்சு [Mime]') {
				this.setState({
					event_name: 'சிரிச்சா போச்சு [Mime]',
					mincount: 6,
					maxcount: 9,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'நண்பேன்டா') {
				this.setState({
					event_name: 'நண்பேன்டா',
					mincount: 2,
					maxcount: 3,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'ச..ரி..க..ம..ப.. [Solo Singing]') {
				this.setState({
					event_name: 'ச..ரி..க..ம..ப.. [Solo Singing]',
					mincount: 1,
					maxcount: 2,
					count: 1,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'ச..ரி..க..ம..ப.. [Group Singing]') {
				this.setState({
					event_name: 'ச..ரி..க..ம..ப.. [Group Singing]',
					mincount: 4,
					maxcount: 8,
					count: 2,
					roll: {},
					name: {},
				});
			}
			else if (e.target.value == 'கலைமாமணி') {
				this.setState({
					event_name: 'கலைமாமணி',
					mincount: 2,
					maxcount: 3,
					count: 2,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'Village விஞ்ஞானி') {
				this.setState({
					event_name: 'Village விஞ்ஞானி',
					mincount: 1,
					maxcount: 2,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'ஆளப்போறான் தமிழன்') {
				this.setState({
					event_name: 'ஆளப்போறான் தமிழன்',
					mincount: 1,
					maxcount: 2,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'MR & MS ORLIA') {
				this.setState({
					event_name: 'MR & MS ORLIA',
					mincount: 1,
					maxcount: 2,
					count: 3,
					roll: {},
					name: {},
				});
			} else if (e.target.value == 'ஒரு கதை சொல்லட்டா Sir?') {
				this.setState({
					event_name: 'ஒரு கதை சொல்லட்டா Sir?',
					mincount: 1,
					maxcount: 7,
					count: 1,
					roll: {},
					name: {},
				});
			}
		} else {
			this.setState({
				nameerror: 'Registration Closed',
			});
		}
	}
	onChangeCount(e) {
		this.setState({
			InputCount: e.target.value,
			name: {},
			roll: {},
			nameerror: '',
			rollerror: '',
			eventerror: '',
			counterror: '',
			teamnameerror: '',
		});
		$('.change').val('');
	}
	handleChange = e => {
		let value = e.target.value;
		let check = e.target.name;
		let name = this.state.name;
		let roll = this.state.roll;
		if (e.target.id == 'name') {
			name[check] = value;
			this.setState({
				name: name,
				nameerror: '',
				rollerror: '',
				eventerror: '',
				counterror: '',
				teamnameerror: '',
			});
		} else {
			roll[check] = value;
			this.setState({
				roll: roll,
				nameerror: '',
				rollerror: '',
				eventerror: '',
				counterror: '',
				teamnameerror: '',
			});
		}
	};
	onSubmit(e) {
		e.preventDefault();
		let k = 0;
		this.setState({
			nameerror: '',
			rollerror: '',
			eventerror: '',
			counterror: '',
			teamnameerror: '',
			message: "Validating..."
		});
		if (!this.state.teamname) {
			this.setState({
				teamnameerror: 'Team Name Required',
				message: ""
			});
			$(".teamname").focus();
			k = 1;
		}
		if (!this.state.event_name) {
			this.setState({
				eventerror: 'Choose Event',
				message: ""
			});
			k = 1;
			// $(".eventname").focus();
		}
		if (!this.state.InputCount && this.state.event_name) {
			this.setState({
				counterror: 'Choose Team Count',
				message: ""
			});
			k = 1;
			$(".inputcount").focus();
		}
		if (this.state.InputCount) {
			for (let i = 0; i < this.state.InputCount; i++) {
				var obj = this.state.name['name' + i];
				var obj1 = this.state.roll['roll' + i];
				if (!obj) {
					this.setState({
						nameerror: 'Student Name Required',
						message: ""
					});
					$(".name0").focus();
					k = 1;
				} else if (!/^[a-zA-Z. ]{2,30}$/.test(obj)) {
					this.setState({
						nameerror: 'Invalid Student Name',
						message: ""
					});
					$(".name0").focus();
					k = 1;
				}
				if (!obj1) {
					this.setState({
						rollerror: 'Register Number Required',
						message: ""
					});
					$(".roll0").focus();
					k = 1;
				} else if (
					!/^[1]{1,1}[6578]{1,1}[bmBM]{1,1}[mbaeclitvsMEVLBACITS]{2,2}[1324]{1,1}[0123456]{1,1}[0-9]{2,2}$/.test(
						obj1
					)
				) {
					this.setState({
						rollerror: 'Invalid Register Number',
						message: ""
					});
					$(".roll0").focus();
					k = 1;
				}
			}
		}
		var keys = Object.keys(this.state.roll);
		var dupe = false;
		for (var i = 0; i < keys.length; i++) {
			for (var j = i + 1; j < keys.length; j++) {
				if (this.state.roll[keys[i]] === this.state.roll[keys[j]]) {
					dupe = true;
					break;
				}
			}
			if (dupe) {
				this.setState({
					rollerror: 'Duplicate Roll Number',
					message: ""
				});
				k = 1;
				$(".roll").focus();
				break;
			}
		}
		var keys = Object.keys(this.state.name);
		var dupe = false;

		for (var i = 0; i < keys.length; i++) {
			for (var j = i + 1; j < keys.length; j++) {
				if (this.state.name[keys[i]] === this.state.name[keys[j]]) {
					dupe = true;
					break;
				}
			}
			if (dupe) {
				this.setState({
					nameerror: 'Duplicate Name',
					message: ""
				});
				$(".name").focus();
				k = 1;
				break;
			}
		}
		//time
		k = 1
		//time
		if (k == 0) {
			this.setState({
				message: "Loading..."
			});
			const data = {
				teamname: this.state.teamname,
				event_name: this.state.event_name,
				inputcount: this.state.InputCount,
				name: this.state.name,
				roll: this.state.roll
			};
			var self = this;
			axios
				.post('/backend/postdata', data)
				.then(function (res) {
					if (res.data == "saved") {
						self.setState({
							event_name: '',
							mincount: '',
							maxcount: '',
							count: '',
							InputCount: '',
							name: {},
							roll: {},
							nameerror: '',
							rollerror: '',
							eventerror: '',
							counterror: '',
							teamname: '',
							teamnameerror: '',
							message: "Registration Successful"
						});
						alert("Registration Successful");
						// $('.lgx-form-msg').removeClass('alert-danger').addClass('alert-success').text("Registration Successful");
						// $('#lgx-form-modal').addClass('sc');
						// $('#lgx-form-modal').addClass('in');		
					} else {
						self.setState({
							message: res.data
						});
						alert(res.data);
						// $('.lgx-form-msg').removeClass('alert-success').addClass('alert-danger').text(res.data);
						// $('#lgx-form-modal').addClass('sc');
						// $('#lgx-form-modal').addClass('in');
					}
				})
				.catch(function (error) { });
		}
	}
	render() {
		let options = [];
		let options1 = [];
		for (let i = this.state.mincount; i < this.state.maxcount; i += 1) {
			options.push(i);
		}
		for (let j = 0; j < this.state.InputCount; j += 1) {
			options1.push(j);
		}
		return (
			<main>
				<div className="lgx-page-wrapper">
					<section>
						<div className="container">

							<h3 style={{ color: "red" }}>**Note: No On-Spot Registration</h3>
							<h3 style={{ color: "red" }}>**Note: Last Date For Registration 12.03.2019 06.00 PM</h3>
							<div className="row">
								<div className="col-sm-12 col-md-6 col-md-offset-3">
									{(this.state.teamnameerror && (this.state.time == "off")) && (
										<div className="alert alert-danger">
											<strong>{this.state.teamnameerror}</strong>
										</div>
									)}
									{(this.state.eventerror && (this.state.time == "off")) && (
										<div className="alert alert-danger">
											<strong>{this.state.eventerror}</strong>
										</div>
									)}
									{(this.state.nameerror && (this.state.time == "off")) && (
										<div className="alert alert-danger">
											<strong>{this.state.nameerror}</strong>
											<br />
										</div>
									)}
									{(this.state.rollerror && (this.state.time == "off")) && (
										<div className="alert alert-danger">
											<strong>{this.state.rollerror}</strong>
											<br />
										</div>
									)}
									{(this.state.counterror && (this.state.time == "off")) && (
										<div className="alert alert-danger">
											<strong>{this.state.counterror}</strong>
										</div>
									)}
									{(this.state.message && (this.state.time == "off")) && (
										<div className="alert alert-success">
											<strong>{this.state.message}</strong>
										</div>
									)}

									{this.state.time == "on" && (
										<div className="alert alert-danger">
											<strong>Registration Closed</strong>
											<br />
										</div>
									)}
									<form onSubmit={this.onSubmit} className="lgx-contactform">
										<div className="form-group">
											<input
												type="text"
												name="lgxname"
												className="form-control teamname lgxname"
												id="lgxname"
												disabled
												value={this.state.teamname}
												onChange={this.onChangeTeamname}
												placeholder="Enter Your Team Name"
											/>
										</div>

										<div className="form-group">
											<select
												disabled
												onChange={this.onChangeEvent}
												className="form-control eventname lgxemail"
												value={this.state.event_name}

											>
												<option value="">Choose</option>
												<option value="Rock n roll">Rock n' roll (GROUP DANCE)</option>
												<option value="இரட்டைக் ௧திரே">இரட்டைக் ௧திரே (TWIN ATTACK)</option>
												<option value="சித்திரம் பேசுதடி">சித்திரம் பேசுதடி [PENCIL SKETCHING]</option>
												<option value="கண்ணாமூச்சி ரே.. ரே..">கண்ணாமூச்சி ரே.. ரே.. [DUMB CHARADES]</option>
												<option value="நெருப்பின்றி புகையாது">நெருப்பின்றி புகையாது [FIRELESS COOKING]</option>
												<option value="சிரிச்சா போச்சு [Comedy]">சிரிச்சா போச்சு[Comedy]</option>
												<option value="சிரிச்சா போச்சு [Mime]">சிரிச்சா போச்சு[Mime]</option>
												<option value="நண்பேன்டா">நண்பேன்டா [BEST BUDDIES]</option>
												<option value="ச..ரி..க..ம..ப.. [Solo Singing]">ச..ரி..க..ம..ப.. [Solo Singing]</option>
												<option value="ச..ரி..க..ம..ப.. [Group Singing]">ச..ரி..க..ம..ப.. [Group Singing]</option>
												<option value="கலைமாமணி">கலைமாமணி[Newspaper Dressing,Hair Dressing,Mehandhi,Nail Art,Face Painting]</option>
												<option value="ஆளப்போறான் தமிழன்">ஆளப்போறான் தமிழன்/Basically I'm from  California</option>
												<option value="Village விஞ்ஞானி">Village விஞ்ஞானி[Arts From Waste]</option>
												<option value="MR & MS ORLIA">MR & MS ORLIA</option>
												<option value="ஒரு கதை சொல்லட்டா Sir?">ஒரு கதை சொல்லட்டா Sir? [Short Film]</option>
											</select>
										</div>
										<div className="form-group">
											{this.state.count && (
												<select
													onChange={this.onChangeCount}
													value={this.state.InputCount}
													className="form-control inputcount lgxsubject"
												>
													<option value="">Select Your Team</option>
													{options.map(i => {
														return (
															<option value={i} key={i}>
																{i}
															</option>
														);
													})}
												</select>
											)}
										</div>
										<div className="form-group">
											{this.state.InputCount &&
												options1.map(i => {
													return (
														<div key={i}>
															<div className="form-group">
																<input
																	onChange={this.handleChange}
																	type="text"
																	id="name"
																	className={`name${i} form-control lgxsubject name change`}
																	name={`name${i}`}
																	placeholder="Name"
																/>
															</div>
															<div className="form-group">
																<input
																	onChange={this.handleChange}
																	type="text"
																	className={`roll${i} form-control lgxsubject name change`}
																	name={`roll${i}`}
																	placeholder="RollNumber"
																/>
															</div>
														</div>
													);
												})}
										</div>
										<button
											type="submit"
											value="contact-form"
											className="lgx-btn hvr-glow hvr-radial-out lgxsend lgx-send"
											disabled
										>
											<span>Register Now</span>
										</button>
									</form>

									<div
										id="lgx-form-modal"
										className="modal fade lgx-form-modal"
										tabIndex="-1"
										role="dialog"
										aria-hidden="true"
									>
										<div className="modal-dialog modal-lg">
											<div className="modal-content lgx-modal-content">
												<div className="modal-header lgx-modal-header">
													<button
														type="button"
														className="close closing brand-color-hover"
														data-dismiss="modal"
														aria-label="Close"
													>
														<i className="fa fa-power-off" />
													</button>
												</div>

												<div className="modal-body">
													<div className="alert lgx-form-msg" role="alert" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</main>
		);
	}
}
export default Registration;
