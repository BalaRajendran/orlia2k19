import React, { Component } from 'react';

import FirstColumn from './Rules/FirstColumn';
import SecondColumn from './Rules/SecondColumn';
import ThirdColumn from './Rules/ThirdColumn';
import FourthColumn from './Rules/FourthColumn';

class Rules extends Component {
    render() {
        return (
            <section>
                <div id="lgx-Event" className="lgx-schedule">
                    <div className="lgx-inner">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="lgx-heading lgx-heading-white">
                                        <h2 className="heading">Event Schedule & Rules</h2>
                                        <h3 className="subheading">
                                            Welcome to Orlia to show your remarkable talent!
											</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="lgx-tab lgx-tab2">
                                        <ul className="nav nav-pills lgx-nav lgx-nav-nogap lgx-nav-colorful">
                                            <li className="active">
                                                <a data-toggle="pill" href="#home">
                                                    <h3>
                                                        Morning Events
                                                    </h3>{' '}
                                                    <p>
                                                        <span>13 </span>March ,2019
														</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-toggle="pill" href="#menu1">
                                                    <h3>
                                                        Mixed Events
                                                    </h3>{' '}
                                                    <p>
                                                        <span>13 </span>March ,2019
														</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-toggle="pill" href="#menu2">
                                                    <h3>
                                                        Afternoon Events
                                                    </h3>{' '}
                                                    <p>
                                                        <span>13 </span>March ,2019
														</p>
                                                </a>
                                            </li>
                                            <li>
                                                <a data-toggle="pill" href="#menu3">
                                                    <h3>
                                                        OPEN STAGE Events
                                                    </h3>{' '}
                                                    <p>
                                                        <span>13 </span>March ,2019
														</p>
                                                </a>
                                            </li>
                                        </ul>
                                        <div className="tab-content lgx-tab-content">
                                            <FirstColumn />
                                            <SecondColumn />
                                            <ThirdColumn />
                                            <FourthColumn />                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="section-btn-area Event List-btn-area">
                                <a target="_blank" className="lgx-btn lgx-btn-red lgx-btn-big" href="assets/pdf/orlia.pdf" download><span>Download Event List (PDF)</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default Rules;