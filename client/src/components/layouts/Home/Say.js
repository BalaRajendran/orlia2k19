import React, { Component } from 'react';

class Say extends Component {
    render() {
        return (
            <section>
                <div id="lgx-testimonial" className="lgx-testimonial">
                    <div className="lgx-inner">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="lgx-heading">
                                        <h2 className="heading">What Clients Say</h2>
                                        <h3 className="subheading">Welcome to the dedicated to building remarkable Testimonials!</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div id="lgx-owltestimonial" className="lgx-owltestimonial lgx-owlnews">
                                    <div className="item">
                                        <blockquote className="lgx-testi-single">
                                            <p><span>This is the best Event Organization in the world</span> Proin sodales dapibus magna, et porta leo convallis sed. Duis tincidunt libero ut neque mollis dignissim. Nullam ultricies sit amet quam non iaculis. Curabitur convallis nulla non nibh aliquet rhoncus. Donec at tempus felis.</p>
                                            <div className="author">
                                                <img src="assets/img/testimonials/author1.jpg" alt="author" />
                                                <h4 className="title"><a href="#"></a>Jonathon Doe</h4>
                                                <div className="rate">
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <div className="item">
                                        <blockquote className="lgx-testi-single">
                                            <p><span>This is the best Event Organization in the world</span> Proin sodales dapibus magna, et porta leo convallis sed. Duis tincidunt libero ut neque mollis dignissim. Nullam ultricies sit amet quam non iaculis. Curabitur convallis nulla non nibh aliquet rhoncus. Donec at tempus felis.</p>
                                            <div className="author">
                                                <img src="assets/img/testimonials/author1.jpg" alt="author" />
                                                <h4 className="title"><a href="#"></a>Jonathon Doe</h4>
                                                <div className="rate">
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    <div className="item">
                                        <blockquote className="lgx-testi-single">
                                            <p><span>This is the best Event Organization in the world</span> Proin sodales dapibus magna, et porta leo convallis sed. Duis tincidunt libero ut neque mollis dignissim. Nullam ultricies sit amet quam non iaculis. Curabitur convallis nulla non nibh aliquet rhoncus. Donec at tempus felis.</p>
                                            <div className="author">
                                                <img src="assets/img/testimonials/author1.jpg" alt="author" />
                                                <h4 className="title"><a href="#"></a>Jonathon Doe</h4>
                                                <div className="rate">
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star active"></i>
                                                    <i className="fa fa-star"></i>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default Say;