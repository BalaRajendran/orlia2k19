import React from 'react';
import { NavLink } from 'react-router-dom';

const Banner = props => {
	return (
		<section>
			<div id="lgx-registration" className="lgx-registration-simple">
				<div className="lgx-inner">
					<div className="container">
						<div className="row">
							<div className="col-xs-12">
								<div className="lgx-registration-area-simple">
									<div className="lgx-heading lgx-heading-white">
										<h2 className="heading">Register Now!</h2>
										<h3 className="subheading">
											Experience the conference wherever you are. Register now
											for online access. Tune in live for the
											<br />
											keynotes and watch sessions on demand. Also be sure to
											join our event
										</h3>
									</div>
									<NavLink className="lgx-btn lgx-btn-red" to="/register">
										Registration
                                    </NavLink>
                                    <NavLink  className="lgx-btn lgx-btn-red" to="/contact">
                                        Contact Us
									</NavLink>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};
export default Banner;
