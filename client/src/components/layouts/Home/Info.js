import React, { Component } from 'react';
import axios from 'axios';

class Info extends Component {
	constructor(props) {
		super(props);
	}
	// componentWillMount() {
	// 	var self = this;
	// 	axios.get('/backend/regcount').then(function(res) {
	// 		self.setState({
	// 			rcount:
	// 				res.data['result'] === 'number'
	// 					? parseInt(res.data['result'])
	// 					: res.data['result'],
	// 			ccount:
	// 				res.data['result1'] === 'number'
	// 					? parseInt(res.data['result1'])
    //                     : res.data['result1'],
    //             visitors: res.data['visitors'] === 'number'
    //                 ? parseInt(res.data['visitors'])
    //                 : res.data['visitors']
	// 		});
	// 	});
	// }
	render() {
		return (
			<section>
				<div id="lgx-milestone-about" className="lgx-milestone-about">
					<div className="lgx-inner">
						<div className="container">
							<div className="row">
								<div className="col-md-12">
									<div className="lgx-milestone-area">
										<div className="lgx-milestone">
											<div className="milestone-inner">
												<div className="lgx-content">
													<div className="row">
														<div className="col-sm-3">
															<div className="lgx-counter-area">
																<img
																	src="assets/img/icons/1.png"
																	alt="teacher icon"
																/>
																<div className="counter-text">
																	<span className="lgx-counter">{this.props.visitors}</span>
																	<small>Visitors Count</small>
																</div>
															</div>
														</div>
														<div className="col-sm-3">
															<div className="lgx-counter-area">
																<img
																	src="assets/img/icons/2.png"
																	alt="teacher icon"
																/>
																<div className="counter-text">
																	<span className="lgx-counter">
																		{this.props.pcount}
																	</span>
																	<small>Registration Count</small>
																</div>
															</div>
														</div>
														<div className="col-sm-3">
															<div className="lgx-counter-area">
																<img
																	src="assets/img/icons/3.png"
																	alt="teacher icon"
																/>
																<div className="counter-text">
																	<span className="lgx-counter">
																		{this.props.tcount}
																	</span>
																	<small>Registration Teams</small>
																</div>
															</div>
														</div>
														<div className="col-sm-3">
															<div className="lgx-counter-area">
																<img
																	src="assets/img/icons/4.png"
																	style={{ height: '128px', width: '128px' }}
																	alt="teacher icon"
																/>
																<div className="counter-text">
																	<span className="lgx-counter">40</span>
																	<small>Student Coordinatos</small>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}
export default Info;
