import React, { Component } from 'react';

class FourthColumn extends Component {
	render() {
		return (
			<div id="menu3" className="tab-pane fade">
				<div
					className="panel-group"
					id="accordion4"
					role="tablist"
					aria-multiselectable="true"
				>
					<div className="panel panel-default lgx-panel">
						<div className="panel-heading" role="tab" id="headingThree4">
							<div className="panel-title">
								<a
									className="collapsed"
									role="button"
									data-toggle="collapse"
									data-parent="#accordion4"
									href="#collapseThree4"
									aria-expanded="true"
									aria-controls="collapseThree4"
								>
									<div className="lgx-single-schedule">
										<div className="author">
											<img src="assets/img/events/14.jpg" alt="logo" />
										</div>
										<div className="schedule-info">
											<h4 className="time">
												10:30 <span>AM</span> - 04.30 <span>PM</span>
											</h4>
											<h3 className="title">ஒரு கதை சொல்லட்டா Sir?</h3>
											<h4 className="author-info">
												<span>Shortfilm</span>
											</h4>
											<a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div
							id="collapseThree4"
							className="panel-collapse collapse"
							role="tabpanel"
							aria-labelledby="headingThree4"
						>
							<div className="panel-body">
								<p className="text">
									Transfer your thought into parchment and Spielberg your way
									into the minds of the audience by portraying your expressions
									to the world.Be quick on your feet,Get ready to
									transfix,Welcome to THEATRIX!
								</p>
								<h4 className="location">
									<strong>Co-Ordinator:</strong> ASHWIN, EEE{' '}
								</h4>
								<h4 className="location">
									<strong>Venue:</strong> Conference Hall 2{' '}
								</h4>
								<h4 className="location">
									<strong>Rules:</strong>
									<br />
									<div>
										<strong style={{color:"black"}}>
											Submission of ShortFilm to the concerned event coordinator
											should be done on or before 10-03-2019 [ 4.00 pm]
											<br />
											Only selected short films will be displayed in Open stage
											big screen. Remaining will be displayed in conference Hall
											3 (timing will be intimated later, once the videos are
											received)
										</strong>
										<strong><p style={{ color: "red" }}><br/>You may also send your tracks to orlia2k19@gmail.com along with your Student name, Team name and Roll number</p></strong>
										<br/>
										<ul>
											<li>
												Marks will be provided based on story, screenplay and
												camera quality
											</li>
											<li>
												Vulgarities or obscenities of any sort will lead to
												disqualification
											</li>
											<li>Jury decision will be final</li>
										</ul>
									</div>
								</h4>
							</div>
						</div>
					</div>
					<div className="panel panel-default lgx-panel">
						<div className="panel-heading" role="tab" id="headingOne">
							<div className="panel-title">
								<a
									role="button"
									data-toggle="collapse"
									data-parent="#accordion"
									href="#collapseOne"
									aria-expanded="true"
									aria-controls="collapseOne"
								>
									<div className="lgx-single-schedule">
										<div className="author">
											<img src="assets/img/events/15.jpg" alt="logo" />
										</div>
										<div className="schedule-info">
											<h4 className="time">
												10:30 <span>AM</span> - 04.30 <span>Pm</span>
											</h4>
											<h3 className="title">Rock n' roll</h3>
											<h4 className="author-info">
												<span>Group Dance</span>
											</h4>
											<a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div
							id="collapseOne"
							className="panel-collapse collapse"
							role="tabpanel"
							aria-labelledby="headingOne"
						>
							<div className="panel-body">
								<p className="text">
									Music saves more lives than war. So put down your gun and get
									on the dance floor. Reveal the mystery that music conceals and
									unleash the hidden language of your soul by whipping up a
									potion of passion, talent, precision and synchronization.
									Speak the language of dance at ORLIA'19 Choreonite!
								</p>
								<h4 className="location">
									<strong>Co-Ordinator:</strong> REVAND, MECH / SATHRIYAN, MECH{' '}
								</h4>
								<h4 className="location">
									<strong>Venue:</strong> Open Stage{' '}
								</h4>
								<h4 className="location">
									<strong>Rules:</strong>
									<br />
									<strong style={{ color: "black" }}>
										Submission of track to the concerned event coordinator
										should be done on or before 10-03-2019 [4.00 pm]. And lot
										will selected on 11-03-2019 [4.30pm] at conference hall-3
									</strong><br/>
									<strong><p style={{ color: "red" }}><br/>You may also send your tracks to orlia2k19@gmail.com along with your Student name, Team name and Roll number</p></strong>
									<br/>
									<ul>
										<li>
											Each team should consist minimum of 8 members for group &
											for Twin attack 2 members and the time limit is 3-5
											minutes
										</li>
										<li>
											Participants should report to the event coordinator before
											10 minutes of the commencement of the event
										</li>
										<li>
											Vulgarities or obscenities of any sort will lead to
											disqualification
										</li>
										<li>
											Criteria for the judgment will be on the basis of Song
											selection, coordination, choreography, costumes and
											overall stage presence
										</li>
									</ul>
								</h4>
							</div>
						</div>
					</div>
					<div className="panel panel-default lgx-panel">
						<div className="panel-heading" role="tab" id="headingTwo">
							<div className="panel-title">
								<a
									className="collapsed"
									role="button"
									data-toggle="collapse"
									data-parent="#accordion"
									href="#collapseTwo"
									aria-expanded="true"
									aria-controls="collapseTwo"
								>
									<div className="lgx-single-schedule">
										<div className="author">
											<img src="assets/img/events/16.jpg" alt="logo" />
										</div>
										<div className="schedule-info">
											<h4 className="time">
												10:30 <span>AM</span> - 04:30 <span>PM</span>
											</h4>
											<h3 className="title">இரட்டைக் ௧திரே</h3>
											<h4 className="author-info">
												<span>Twin Attack</span>
											</h4>
											<a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
										</div>
									</div>
								</a>
							</div>
						</div>
						<div
							id="collapseTwo"
							className="panel-collapse collapse"
							role="tabpanel"
							aria-labelledby="headingTwo"
						>
							<div className="panel-body">
								<p className="text">
									Two is always better than one. It is always great to dance
									with your mate. Come with your partner and mesmerize us with
									your groovy moves. Register here to participate in ORLIA's
									Duet Freestyle event.
								</p>
								<h4 className="location">
									<strong>Co-Ordinator:</strong> RAMANAN, MECH / KARTHICK RAJA,
									MECH{' '}
								</h4>
								<h4 className="location">
									<strong>Venue:</strong> Open Stage
								</h4>
								<strong><p style={{ color: "red" }}>You may also send your tracks to orlia2k19@gmail.com along with your Student name, Team name and Roll number</p></strong>
								<h4 className="location">
									<strong>Rules:</strong><br/>
									Submission of track to the concerned event coordinator should be done on or before 
10-03-2019 [4.00 pm]. And lot will selected on 11-03-2019 [4.30pm] at conference hall-3.
									<br /><br/>
									<li>Each team should consist minimum of 8 members for group & for Twin attack 2 members and the time limit is 3-5 minutes</li>
									<li>Participants should report to the event coordinator before 10 minutes of the commencement of the event</li>
									<li>Vulgarities or obscenities of any sort will lead to disqualification</li>
									<li>Criteria for the judgment will be on the basis of Song selection, coordination, choreography, costumes and overall stage presence</li>
								</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default FourthColumn;
