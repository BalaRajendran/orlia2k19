import React, { Component } from 'react';


class FirstColumn extends Component {
    render() {
        return (
            <div id="home" className="tab-pane fade in active">
                <div
                    className="panel-group"
                    id="accordion"
                    role="tablist"
                    aria-multiselectable="true"
                >
                                                    
                    <div className="panel panel-default lgx-panel">
                        <div className="panel-heading" role="tab" id="headingThree">
                            <div className="panel-title">
                                <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                                    aria-expanded="true" aria-controls="collapseThree">
                                    <div className="lgx-single-schedule">
                                        <div className="author">
                                            <img src="assets/img/events/1.jpg" alt="logo" />
                                        </div>
                                        <div className="schedule-info">
                                            <h4 className="time">09:30 <span>Am</span> - 10.30 <span>Am</span></h4>
                                            <h3 className="title">சித்திரம் பேசுதடி</h3>
                                            <h4 className="author-info"><span>Pencil Sketching</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="collapseThree" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="panel-body">
                                <p className="text">
                                    A gentle stroke of the brush, a tinge of glitter and an overflowing imagination. This is all that is
                                    needed to paint and make it stand out in a crowd. Add the final touches and awe the audience with its
                                    beauty. Face it people, it does not get better than this.
                                                    </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> FAREZA NACHIAR, CSE </h4>
                                <h4 className="location"><strong>Venue:</strong> Drawing Hall 2 </h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Required materials should be brought by the participant.</li>
                                        <li>Timing: One hour</li>
                                        <li>Evaluation will be based on creativity, sketching and style</li>
                                        <li>Own theme allowed</li>
                                        <li>Marks will be provided based on sketching and theme</li>
                                        <li>Jury decision will be final</li>
                                    </ul>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default lgx-panel">
                        <div className="panel-heading" role="tab" id="headingfour">
                            <div className="panel-title">
                                <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour"
                                    aria-expanded="true" aria-controls="collapsefour">
                                    <div className="lgx-single-schedule">
                                        <div className="author">
                                            <img src="assets/img/events/2.png" alt="logo" />
                                        </div>
                                        <div className="schedule-info">
                                            <h4 className="time">09:30 <span>Am</span> - 11.30 <span>Am</span></h4>
                                            <h3 className="title">கண்ணாமூச்சி ரே.. ரே.. </h3>
                                            <h4 className="author-info"><span>Dumb C</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="collapsefour" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="panel-body">
                                <p className="text">
                                    Originally, the game was a dramatic form of literary charades: a single person would act out each
                                    syllable of a word or phrase in order, followed by the whole phrase together, while the rest of the
                                    group guessed. A variant was to have teams who acted scenes out together while the others guessed.
                                                    </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> NISHANTH, EEE </h4>
                                <h4 className="location"><strong>Venue:</strong> PG Meeting Hall </h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Two members per team</li>
                                        <li>Preliminary rounds will be conducted</li>
                                        <li>Jury decision will be final</li>
                                    </ul>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default lgx-panel">
                        <div className="panel-heading" role="tab" id="headingfive">
                            <div className="panel-title">
                                <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive"
                                    aria-expanded="true" aria-controls="collapsefive">
                                    <div className="lgx-single-schedule">
                                        <div className="author">
                                            <img src="assets/img/events/3.jpg" alt="logo" />
                                        </div>
                                        <div className="schedule-info">
                                            <h4 className="time">10:30 <span>Am</span> - 12.30 <span>Pm</span></h4>
                                            <h3 className="title">நெருப்பின்றி பு௧ையாது</h3>
                                            <h4 className="author-info"><span>Fireless Cooking</span> </h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="collapsefive" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="panel-body">
                                <p className="text">
                                    Tired of eating hostel food?? Come here make your own dish, eat to your heart's content.
                                </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> JOTHIVEL, MECH </h4>
                                <h4 className="location"><strong>Venue:</strong> PG Exam Hall </h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Participants should bring the required materials.</li>
                                        <li>Timing: One hours</li>
                                        <li>Evaluation will be based on innovation, taste, presentation and usage of materials</li>
                                        <li>Two members per team </li>
                                        <li>Boiled items are not allowed</li>
                                    </ul>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default lgx-panel">
                        <div className="panel-heading" role="tab" id="headingOne2">
                            <div className="panel-title">
                                <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2"
                                    aria-expanded="true" aria-controls="collapseOne2">
                                    <div className="lgx-single-schedule">
                                        <div className="author">
                                            <img src="assets/img/events/4.jpg" alt="logo" />
                                        </div>
                                        <div className="schedule-info">
                                            <h4 className="time">10:30 <span>Am</span> - 12.30 <span>Pm</span></h4>
                                            <h3 className="title">சிரிச்சா போச்சு</h3>
                                            <h4 className="author-info"><span>Comedy & Mime</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="collapseOne2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div className="panel-body">
                                <p className="text">
                                    If you are the type of person that can walk up to anyone and tickle their funny bone, then this event is
                                    right up your alley. ORLIA presents Kalakkal Kalatta, the Tamil Stand-up comedy event. Send the audience
                                    into fits of laughter using your boundless creativity, humour and mimicry skills to be adjudged the next
                                    comedic genius.
                                                    </p>
                                <strong><p style={{ color: "red" }}>You may also send your tracks to orlia2k19@gmail.com along with your Student name, Team name and Roll number</p></strong>
                                <h4 className="location"><strong>Co-Ordinator:</strong> SHRUTI RAJAVEL, ECE </h4>
                                <h4 className="location"><strong>Venue:</strong> Conference Hall 2 </h4>
                                <h4 className="location"><strong>Rules:</strong><br/>
                                    <strong>Comedy:</strong>
                                    <ul>
                                        <li>Each team should consist of 2-5 members and the time limit is 6-8 minutes</li>
                                        <li>Judgment will be based on the creativity, voice clarity and humor content</li>
                                        <li>The decision of the judges will be final and irrevocable</li>
                                    </ul>
                                    <strong>MIME:</strong>
                                    <ul>
                                        <li>Submission of track to the concerned event coordinator should be done on or before 10-03-2019 [4.00 pm]</li>
                                        <li>Each team should consist of 6-8 members and the time limit is 8-10 minutes</li>
                                        <li>There should be a proper theme for the play/skit and should not contain any vulgarity</li>
                                        <li>Criteria for the judgment will be based on music and synchronization, coordination, costumes, concept</li>
                                    </ul>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default FirstColumn;