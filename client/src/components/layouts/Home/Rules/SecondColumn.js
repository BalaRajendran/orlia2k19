import React, { Component } from 'react';

class SecondColumn extends Component {
	render() {
		return (
			<div id="menu1" className="tab-pane fade">
				<div
					className="panel-group"
					id="accordion2"
					role="tablist"
					aria-multiselectable="true"
				>
                        <div className="panel panel-default lgx-panel">
                            <div className="panel-heading" role="tab" id="headingTwo4">
                                <div className="panel-title">
                                    <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseTwo4"
                                        aria-expanded="true" aria-controls="collapseTwo4">
                                        <div className="lgx-single-schedule">
                                            <div className="author">
                                                <img src="assets/img/events/5.jpg" alt="logo" />
                                            </div>
                                            <div className="schedule-info">
                                                <h3 className="title">Mr & Ms ORLIA </h3>
                                            <h4 className="author-info"><span></span> </h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseTwo4" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo4">
                                <div className="panel-body">
                                    <p className="text">
                                        "Beauty only captures attention. Personality is what captures the heart." If you think you have got the
                                    wits and the guts to be out there on stage and prove to the world that you are an all-rounder, then join
                                    us in the ultimate fight to be crowned as Mr and Miss ORLIA!
                                    </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> RAJASREE, EEE / BALAJI, MECH </h4>
                                <h4 className="location"><strong>Perlims Time:</strong> 11:00 AM - 12.15 PM </h4>
                                <h4 className="location"><strong>Finals:</strong> 02:30 PM - 04.30 PM </h4>
                                <h4 className="location"><strong>Venue:</strong> Conference Hall 3 </h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Solo event</li>
                                        <li>Tasks will be given on spot</li>
                                        <li>Jury's decision will be final</li>
                                    </ul>
                                </h4>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default lgx-panel">
                            <div className="panel-heading" role="tab" id="headingTwo2">
                                <div className="panel-title">
                                    <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo2"
                                        aria-expanded="true" aria-controls="collapseTwo2">
                                        <div className="lgx-single-schedule">
                                            <div className="author">
                                                <img src="assets/img/events/6.jpg" alt="logo" />
                                            </div>
                                            <div className="schedule-info">
                                                <h4 className="time">11:30 <span>AM</span> - 01.30 <span>PM</span></h4>
                                                <h3 className="title">நண்பேன்டா</h3>
                                            <h4 className="author-info"><span>Best Buddies</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseTwo2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div className="panel-body">
                                    <p className="text">
                                        Life was meant for good friends and great adventures. I would rather walk with a friend in the dark,
                                    than alone in the light. A good friend is like a four-leaf clover: hard to find and lucky to have. There
                                    is nothing I would not do for those who are really my friends.
                                                    </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> ANANDH / IT </h4>
                                <h4 className="location"><strong>Venue:</strong> Conference Hall 5 </h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Two members per team</li>
                                        <li>Preliminary rounds will be conducted</li>
                                        <li>Jury decision will be final</li>
                                    </ul>
                                </h4>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default lgx-panel">
                            <div className="panel-heading" role="tab" id="headingThree2">
                                <div className="panel-title">
                                    <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree2"
                                        aria-expanded="true" aria-controls="collapseThree2">
                                        <div className="lgx-single-schedule">
                                            <div className="author">
                                                <img src="assets/img/events/7.jpg" alt="logo" />
                                            </div>
                                            <div className="schedule-info">
                                                <h4 className="time">12:30 <span>PM</span> - 02.30 <span>PM</span></h4>
                                                <h3 className="title">ச..ரி..க..ம..ப.. </h3>
                                            <h4 className="author-info"><span>Singing</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseThree2" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div className="panel-body">
                                    <p className="text">
                                        Do RE Me Fa So La Come croon, groove, clap and move with us this ORLIA in Capella!
                                                    </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> NIRANJAN, CIVIL </h4>
                                <h4 className="location"><strong>Venue:</strong> Conference Hall 4 </h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>A team should have 4-6 members (Group Singing)</li>
                                        <li style={{listStyleType: "none"}}>
                                            <ul>
                                                <li>Round 1 : Blind audition( Any song)</li>
                                                <li>Round 2 : Grand finale ( Any song)</li>
                                            </ul>
                                        </li>
                                        <li>Time duration – 3 to 5 mins</li>
                                        <li>Karoake are allowed</li>
                                        <li>Jury's decision will be final</li>
                                    </ul>
                                </h4>
                                </div>
                            </div>
                        </div>
                        <div className="panel panel-default lgx-panel">
                            <div className="panel-heading" role="tab" id="headingOne3">
                                <div className="panel-title">
                                    <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseOne3" aria-expanded="true" aria-controls="collapseOne3">
                                        <div className="lgx-single-schedule">
                                        <div className="author author-multi">
                                            <img src="assets/img/events/8.jpg" alt="logo" />
                                            <img src="assets/img/events/9.jpg" alt="logo" />
                                            <img src="assets/img/events/10.jpg" alt="logo" />
                                            <img src="assets/img/events/17.jpg" alt="logo" />
                                            </div>
                                            <div className="schedule-info">
                                                <h4 className="time">12:30 <span>Pm</span> - 01.30 <span>Pm</span></h4>
                                                <h3 className="title">கலைமாமணி</h3>
                                            <h4 className="author-info"><span>Newspaper Dressing,Hair Dressing,Mehandhi,Nail Art,Face Painting</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div id="collapseOne3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div className="panel-body">
                                    <p className="text">
                                        Need a platform to exhibit your talents?? Here's the floor to show N number of skills under a roof.Get the "Nail furnished", "Palm" "Mehandied", "Face Painted" and many more.... GET READY AND RUSH HERE...
                                                    </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> BAVITHRA, IT </h4>
                                <h4 className="location"><strong>Venue:</strong>  Drawing Hall 1</h4>
                                <h4 className="location"><strong>Rules:</strong>
                                <strong>NEWSPAPER DRESSING </strong><br/>
                                    <ul>
                                        <li>Time duration – 1 hour</li>
                                        <li>Two members per team</li>
                                        <li>Newspaper, tape, gum, scissors, rope are allowed and should be brought by the participant</li>
                                        <li>Participants should not leave any materials inside the hall</li>
                                    </ul>
                                    <strong>HAIR DRESSING</strong><br/>
                                     <ul>
                                        <li>Time duration – 1 hour</li>
                                        <li>Two members per team</li>
                                        <li>Materials will not be provided</li>
                                        <li>Participants should not leave any material inside the hall</li>
                                    </ul>
                                    <strong>Nail art, Mehandi, Face painting</strong>
                                    <ul>
                                    <li>Participation – 2 members</li>
                                    <li>Materials must be brought by your own</li>
                                    <li>Jury's decision will be final</li>
                                    </ul>
                                </h4>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
		);
	}
}
export default SecondColumn;
