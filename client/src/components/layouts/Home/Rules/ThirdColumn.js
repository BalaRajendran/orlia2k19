import React, { Component } from 'react';

class ThirdColumn extends Component {
    render() {
        return (
            <div id="menu2" className="tab-pane fade">
                <div
                    className="panel-group"
                    id="accordion3"
                    role="tablist"
                    aria-multiselectable="true"
                >
                    <div className="panel panel-default lgx-panel">
                        <div className="panel-heading" role="tab" id="headingThree3">
                            <div className="panel-title">
                                <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree3"
                                    aria-expanded="true" aria-controls="collapseThree3">
                                    <div className="lgx-single-schedule">
                                        <div className="author">
                                            <img src="assets/img/events/12.jpg" alt="logo" />
                                        </div>
                                        <div className="schedule-info">
                                            <h4 className="time">01:30 <span>PM</span> - 03.30 <span>PM</span></h4>
                                            <h3 className="title">ஆளப்போறான் தமிழன்/Basically I'm from  California [EXTERMPORE Tamil / English]</h3>
                                            <h4 className="author-info"><span>English Speech and Tamil Speech</span> </h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="collapseThree3" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div className="panel-body">
                                <p className="text">
                                    Are you one of those people that can speak about anything, anywhere and at any time? Are you one of
                                    those people that can pull off a presentation without even preparing? If you are that person, then this
                                    is definitely the event for you. This ORLIA we are giving you the mike and a chance to win exciting
                                    prizes.
                                </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> AMAL, MECH / NAVEEN RAJA, CIVIL </h4>
                                <h4 className="location"><strong>Venue:</strong> Conference Hall 1</h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Topic will be given on the spot</li>
                                        <li>This is a solo event</li>
                                        <li>Participants are expected to speak for a maximum of three minutes relevant to the topic</li>
                                        <li>Delivery of speech may be in any language [Tamil / English]</li>
                                        <li>Mobile phone usage is not allowed in the venue</li>
                                        <li>Jury's decision will be final</li>
                                    </ul>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div className="panel panel-default lgx-panel">
                        <div className="panel-heading" role="tab" id="headingOne4">
                            <div className="panel-title">
                                <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseOne4"
                                    aria-expanded="true" aria-controls="collapseOne4">
                                    <div className="lgx-single-schedule">
                                        <div className="author">
                                            <img src="assets/img/events/13.jpg" alt="logo" />
                                        </div>
                                        <div className="schedule-info">
                                            <h4 className="time">01:30 <span>PM</span> - 02.30 <span>PM</span></h4>
                                            <h3 className="title">Village விஞ்ஞானி</h3>
                                            <h4 className="author-info"><span>Art From Waste</span></h4>
                                            <a className="author-info" href="#!"><h4 className="author-info"><span style={{ color: "#554bb9" }}>Click Here For Rules</span></h4></a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div id="collapseOne4" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne4">
                            <div className="panel-body">
                                <p className="text">
                                    Art is not what you see but what you make others see. It's not just about Creativity, It is about the
                                    person you're becoming while you're creating.
                                </p>
                                <h4 className="location"><strong>Co-Ordinator:</strong> KEERTHI, ECE </h4>
                                <h4 className="location"><strong>Venue:</strong> Drawing Hall 3</h4>
                                <h4 className="location"><strong>Rules:</strong>
                                    <ul>
                                        <li>Solo event</li>
                                        <li>Students should bring the required materials.</li>
                                        <li>Final products must be made on the spot</li>
                                    </ul>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default ThirdColumn;