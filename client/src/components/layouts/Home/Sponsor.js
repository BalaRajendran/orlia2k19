import React, { Component } from 'react';

class Sponsor extends Component {
    render() {
        return (
            <section>
                <div id="lgx-sponsors" className="lgx-sponsors">
                    <div className="lgx-inner-bg">
                        <div className="lgx-inner">
                            <div className="container">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <div className="lgx-heading">
                                            <h2 className="heading">Offcial Sponsonrs</h2>
                                            <h3 className="subheading">
                                                Welcome to the dedicated to building remarkable
                                                Sponsores!
												</h3>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12">
                                        <h3 className="sponsored-heading first-heading">
                                            Gold Sponsors
											</h3>
                                        <div className="sponsors-area sponsors-area-nogap sponsors-area-noshadow sponsors-area-col3">
                                            <div className="single">
                                                <a className="" href="#">
                                                    <img
                                                        src="assets/img/sponsors/sponsor-sp3.png"
                                                        alt="sponsor"
                                                    />
                                                </a>
                                            </div>
                                            <div className="single">
                                                <a className="" href="#">
                                                    <img
                                                        src="assets/img/sponsors/sponsor-sp2.png"
                                                        alt="sponsor"
                                                    />
                                                </a>
                                            </div>
                                            <div className="single">
                                                <a className="" href="#">
                                                    <img
                                                        src="assets/img/sponsors/sponsor3.png"
                                                        alt="sponsor"
                                                    />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12">
                                        <h3 className="sponsored-heading secound-heading">
                                            Silver Sponsors
											</h3>
                                        <div className="sponsors-area sponsors-area-noshadow sponsors-area-nogap">
                                            <div className="single">
                                                <a className="" href="#">
                                                    <img
                                                        src="assets/img/sponsors/sponsor4.png"
                                                        alt="sponsor"
                                                    />
                                                </a>
                                            </div>
                                            <div className="single">
                                                <a className="" href="#">
                                                    <img
                                                        src="assets/img/sponsors/sponsor-sp1.png"
                                                        alt="sponsor"
                                                    />
                                                </a>
                                            </div>
                                            <div className="single">
                                                <a className="" href="#">
                                                    <img
                                                        src="assets/img/sponsors/sponsor3.png"
                                                        alt="sponsor"
                                                    />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default Sponsor;