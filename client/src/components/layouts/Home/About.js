import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class About extends Component {
    render() {
        return (
            <section>
                <div id="lgx-about" className="lgx-about">
                    <div className="lgx-inner">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12 col-md-7">
                                    <div className="lgx-about-content-area">
                                        <div className="lgx-heading">
                                            <h2 className="heading">Orlia - 2k19</h2>
                                            <h3 className="subheading">
                                                Do not miss the upcoming event
												</h3>
                                        </div>
                                        <div className="lgx-about-content">
                                            <p className="text">
                                                Everyone is born with an idea of some kind of
                                                'PASSION' in their life. Some become dancers,Some
                                                become singers, Some act in plays while others run to
                                                search for their talents and dreams. Everyone are born
                                                equally and with a 'Million Dreams' to pursue. So here
                                                we are to fulfil the one of your dreams with 'The
                                                Greatest Talent show' of MKCE... 'ORLIA 2k19'- Unveil
                                                your potential to showcase the world who you are.
                                                Because tomorrow begins with what you do today....
                                                Count the seconds with us... But don't just keep
                                                counting... Buckle yourself and go register to... THE
                                                GREATEST TALENT SHOW of MKCE 'ORLIA 2K19'
												</p>
                                            <div className="about-date-area">
                                                <h4 className="date">
                                                    <span>13</span>
                                                </h4>
                                                <p>
                                                    <span>March 2019</span> 
                                                    M.Kumarasamy College Of Engineering
                                                </p>
                                            </div>

                                            <div className="section-btn-area">
                                                <NavLink
                                                    className="lgx-btn lgx-btn-red lgx-scroll"
                                                    to="/register#lgx-header"
                                                >
                                                    <span>Register Now</span>
                                                </NavLink>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-12 col-md-5">
                                    <div className="lgx-about-img-sp">
                                        <img src="assets/img/i1.jpeg" alt="about" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default About;