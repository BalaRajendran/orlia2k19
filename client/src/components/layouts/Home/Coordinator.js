import React, { Component } from 'react';

class Coordinator extends Component {
	render() {
		return (
			<section>
				<div id="lgx-news" className="lgx-news">
					<div className="lgx-inner">
						<div className="container">
							<div className="row">
								<div className="col-xs-12">
									<div className="lgx-heading">
										<h2 className="heading">Student Co ordinators</h2>
										<h3 className="subheading">
											Feel free to ask your questions about Orlia Event.
										</h3>
									</div>
								</div> 
							</div>
							<div className="row">
								<div className="col-xs-12 col-sm-6 col-md-3">
									<div className="lgx-single-news">
										<div className="single-news-info">
											<h3 className="title">
												<a href="#!">Mohammed Azaruddin</a>
												<br />
												<h5>+91 98765 43210</h5>
											</h3>
										</div>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-3">
									<div className="lgx-single-news">
										<div className="single-news-info">
											<h3 className="title">
												<a href="#!">Infant Sabin</a>
												<br />
												<h5>+91 99445 93802</h5>
											</h3>
										</div>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-3">
									<div className="lgx-single-news">
										<div className="single-news-info">
											<h3 className="title">
												<a href="#!">Amal Josline</a>
												<br />
												<h5>+91 95854 66634</h5>
											</h3>
										</div>
									</div>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-3">
									<div className="lgx-single-news">
										<div className="single-news-info">
											<h3 className="title">
												<a href="#!">Karthik Raja</a>
												<br />
												<h5>+91 98765 43210</h5>
											</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}
export default Coordinator;
