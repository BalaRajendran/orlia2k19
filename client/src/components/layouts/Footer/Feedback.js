import React, { Component } from 'react';
import axios from 'axios';
var $ = require('jquery');

class Feedback extends Component {
	constructor(props) {
		super(props);
		this.onChangeName = this.onChangeName.bind(this);
		this.onChangePhone = this.onChangePhone.bind(this);
        this.onChangeRollNo = this.onChangeRollNo.bind(this);
		this.onChangeFeedback = this.onChangeFeedback.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
			nameerror: '',
			rollnoerror: '',
			phoneerror: '',
			feedbackerror: '',
			name: '',
			feedback: '',
			phone: '',
			rollno: '',
			message: '',
		};
	}
    onChangeName(e) {
        this.setState({
            nameerror: '',
            rollnoerror: '',
            phoneerror: '',
            feedbackerror: '',
            message: '',
			name: e.target.value,
		});
	}
	onChangePhone(e) {
        this.setState({
            nameerror: '',
            rollnoerror: '',
            phoneerror: '',
            feedbackerror: '',
            message: '',
			phone: e.target.value,
		});
	}
	onChangeRollNo(e) {
        this.setState({
            nameerror: '',
            rollnoerror: '',
            phoneerror: '',
            feedbackerror: '',
            message: '',
			rollno: e.target.value,
		});
	}
	onChangeFeedback(e) {
        this.setState({
            nameerror: '',
            rollnoerror: '',
            phoneerror: '',
            feedbackerror: '',
            message: '',
			feedback: e.target.value,
		});
	}
	onSubmit(e) {
		e.preventDefault();
		let k = 0;
		this.setState({
			nameerror: '',
            rollnoerror: '',
			phoneerror: '',
			feedbackerror: '',
			message: 'Validating...',
		});
        if (!/^[a-zA-Z. ]{2,30}$/.test(this.state.name)) {
            this.setState({
                nameerror: 'Invalid Student Name',
                message: ""
            });
            $(".name").focus();
            k = 1;
        }
        if (!this.state.name) {
            this.setState({
                nameerror: 'Student Name Required',
                message: '',
            });
            $('.name').focus();
            k = 1;
        }
		if (!this.state.rollno) {
			this.setState({
				rollnoerror: 'Student Register Number Required',
				message: '',
			});
			$('.rollno').focus();
			k = 1;
        }
        else if (!/^[1]{1,1}[6578]{1,1}[bmBM]{1,1}[mbaecitsMEBACITS]{2,2}[324]{1,1}[0123456]{1,1}[0-9]{2,2}$/.test(this.state.rollno)
        ) {
            this.setState({
                rollnoerror: 'Invalid Register Number',
                message: ""
            });
            $(".roll0").focus();
            k = 1;
        }
        const re = /^([7-9]{1}[0-9]{9})$/;
        if (re.test(this.state.phone) === false) {
            this.setState({
                phoneerror: 'Invalid Mobile Number',
                message:""
            });
            $(".name").focus();
            k = 1;
        }
        if (!this.state.phone) {
            this.setState({
                phoneerror: 'Phone Number Required',
                message: '',
            });
            $('.phone').focus();
            k = 1;
        }
        if (this.state.feedback.length <= 10) {
            this.setState({
                feedbackerror: 'Feedback / Query must be atlest 10 characters',
                message:""
            });
            $('.feedback').focus();
            k = 1;
        }
        if (!this.state.feedback) {
            this.setState({
                feedbackerror: 'Feedback Required',
                message: '',
            });
            $('.feedback').focus();
            k = 1;
        }
		if (k == 0) {
			this.setState({
				message: 'Loading...',
			});
			const data = {
				name: this.state.name,
				rollno: this.state.rollno,
				phone: this.state.phone,
				message: this.state.feedback,
            };
			var self = this;
			axios
				.post('/backend/feedback', data)
				.then(function(res) {
					if (res.data == 'saved') {
						self.setState({
							nameerror: '',
                            rollnoerror: '',
							phoneerror: '',
							feedbackerror: '',
							message: '',
							name: '',
							feedback: '',
							phone: '',
							rollno: '',
						});
						$('.lgx-form-msg')
							.removeClass('alert-danger')
							.addClass('alert-success')
							.text('We Will Contact You Soon');
						$('#lgx-form-modal').addClass('sc');
						$('#lgx-form-modal').addClass('in');
					} else {
						self.setState({
							message: '',
						});
						$('.lgx-form-msg')
							.removeClass('alert-success')
							.addClass('alert-danger')
							.text(res.data);
						$('#lgx-form-modal').addClass('sc');
						$('#lgx-form-modal').addClass('in');
					}
				})
		}
	}
	render() {
		return (
			<main>
				<div className="lgx-page-wrapper">
					<section>
						<div className="container">
							<h3 style={{ color: "#554bb9" }}>Post Your Query / Feedback</h3><br/>
							<div className="row">
								<div className="col-sm-12 col-md-6 col-md-offset-3">
                                    {this.state.nameerror && (
                                        <div className="alert alert-danger">
                                            <strong>{this.state.nameerror}</strong>
                                        </div>
                                    )}
                                    {this.state.rollnoerror && (
                                        <div className="alert alert-danger">
                                            <strong>{this.state.rollnoerror}</strong>
                                        </div>
                                    )}
                                    {this.state.phoneerror && (
                                        <div className="alert alert-danger">
                                            <strong>{this.state.phoneerror}</strong>
                                        </div>
                                    )}
                                    {this.state.feedbackerror && (
										<div className="alert alert-danger">
											<strong>{this.state.feedbackerror}</strong>
										</div>
									)}
									{this.state.message && (
										<div className="alert alert-success">
											<strong>{this.state.message}</strong>
										</div>
									)}
									<form onSubmit={this.onSubmit}>
										<div className="form-group">
											<input
												type="text"
												name="name"
												value={this.state.name}
												onChange={this.onChangeName}
												className="form-control name lgxname"
												id="lgxname"
												placeholder="Enter Your Student Name"
											/>
										</div>
										<div className="form-group">
											<input
												type="text"
												name="rollno"
												value={this.state.rollno}
												onChange={this.onChangeRollNo}
												className="form-control rollno lgxname"
												id="mail"
												placeholder="Enter Your Register Number"
											/>
										</div>
										<div className="form-group">
											<input
												type="text"
												value={this.state.phone}
												onChange={this.onChangePhone}
												name="phone"
												className="form-control phone lgxname"
												id="phone"
												placeholder="Enter Your Phone Number"
											/>
										</div>
										<div className="form-group">
											<input
												type="text"
												style={{height:"200px"}}
												name="feedback"
												value={this.state.feedback}
												onChange={this.onChangeFeedback}
												className="form-control feedback lgxname"
												id="message"
												placeholder="Query / Feedback"
											/>
										</div>
                                        <button type="submit"
                                            className="lgx-btn hvr-glow hvr-radial-out lgxsend lgx-send">
											<span>Submit Now</span>
										</button>
									</form>

									<div
										id="lgx-form-modal"
										className="modal fade lgx-form-modal"
										tabIndex="-1"
										role="dialog"
										aria-hidden="true"
									>
										<div className="modal-dialog modal-lg">
											<div className="modal-content lgx-modal-content">
												<div className="modal-header lgx-modal-header">
													<button
														type="button"
														className="close brand-color-hover"
													>
														<i className="fa fa-power-off" />
													</button>
												</div>

												<div className="modal-body">
													<div className="alert lgx-form-msg" role="alert" />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</main>
		);
	}
}
export default Feedback;
