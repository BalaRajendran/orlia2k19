import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class Footer extends Component {
	render() {
		return (
			<footer>
				<div id="lgx-contact" className="lgx-footer">
					<div className="lgx-inner-footer">
						<div className="lgx-subscriber-area ">
							<div className="container">
								<div className="lgx-subscriber-inner" />
							</div>
						</div>
						<div className="container">						
							<div className="lgx-footer-area">
								<div className="lgx-footer-single">
									<a className="logo" target="_blank"  href="http://mkce.ac.in/">
										<img style={{marginTop:"39px"}}
											src="assets/img/footer.jpg"
											className="footer-log1o-img"
											alt="Logo"
										/>
									</a>
								</div>
								<div className="lgx-footer-single">
									<h3 className="footer-title">Event Location </h3>
									<h4 className="date">13 March, 2019</h4>
									<address>
										M.Kumarasamy College of Engineering <br />
										Thalavapalayam, Karur - 639113
									</address>
								</div>
								<div className="lgx-footer-single">
									<h3 className="footer-title">Social Connection</h3>
									<p className="text">
										You may connect social area <br /> for Any update
									</p>
									<ul className="list-inline lgx-social-footer">
										<li>
											<a
												target="_blank"
												href="https://www.facebook.com/orlia2k19/"
											>
												<i className="fa fa-facebook" aria-hidden="true" />
											</a>
										</li>
									</ul>
								</div>
								<div className="lgx-footer-single">
									<h3 className="footer-title">Designed By</h3>
									
									<a style={{ color: "#efa506" }} href="https://www.linkedin.com/in/balaji-rajendran-a6ab6513a/" target="_blank">
										<address>
											Balaji R, <br/>
											Prefinal Year,
										<br />Information Technology
												</address>
									</a>
								</div>
							</div>
							<div id="lgx-modal-map" className="modal fade lgx-modal">
								<div className="modal-dialog">
									<div className="modal-content">
										<div className="modal-header">
											<button
												type="button"
												className="close"
												data-dismiss="modal"
												aria-hidden="true"
											>
												×
											</button>
										</div>
										<div className="modal-body">
											<div
												className="lgxmapcanvas map-canvas-default"
												id="map_canvas"
											>
												{' '}
											</div>
										</div>
									</div>
								</div>
							</div>

							<div className="lgx-footer-bottom">
								<div className="lgx-copyright">
									<p>
										{' '}
										<span>©</span> 2019 Orlia is powered by{' '}
										<a href="#!">Masiko Club.</a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		);
	}
}
export default Footer;
