import React, { Component } from 'react';

class TopSlide extends Component {
    render() {
        return (
            <section>
                <div className="lgx-banner">
                    <div className="lgx-banner-style">
                        <div className="lgx-inner lgx-inner-fixed">
                            <div className="container">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <div className="lgx-banner-info-area">
                                            <div className="lgx-banner-info-circle">
                                                <div className="info-circle-inner">
                                                    <h3 className="date">
                                                        <b className="lgx-counter">13</b> <span>March</span>
                                                    </h3>
                                                    <div className="lgx-countdown-area">
                                                        <div id="lgx-countdown" data-date="2019/03/13" />
                                                        <h3 className="date">
                                                            <span>To Go</span>
                                                        </h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="lgx-banner-info">
                                                <h3 className="subtitle">
                                                    A Place to Expose Your Talent
													</h3>
                                                <h2 className="title">
                                                    Orlia -{' '}
                                                    <span>
                                                        <b>2</b>
                                                        <b>k</b>
                                                        <b>1</b>
                                                        <b>9</b>
                                                    </span>
                                                </h2>
                                                <h3 className="location">
                                                    <i className="fa fa-map-marker" />
                                                    &nbsp;M.Kumarasamy College Of Engineering,
                                                       Thalavapalayam, Tamil Nadu
													</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}
export default TopSlide;