import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';

class Navigation extends Component {
    render() {
        return (
            <div id="navbar" className="navbar-collapse collapse">
                <div className="lgx-nav-right navbar-right">
                    <div className="lgx-cart-area">
                        <Link className="lgx-btn lgx-btn-red" to="/register#lgx-header">
                            <span>Register</span>
                        </Link>
                    </div>
                </div>
                <ul className="nav navbar-nav lgx-nav navbar-right">
                    <li>
                        <Link className="lgx-scroll" to="/#lgx-home">
                            Home
						</Link>
                    </li>
                    <li>
                        <Link className="lgx-scroll" to="/#lgx-Event">
                            Schedule
						</Link>
                    </li>

                    <li>
                        <Link className="lgx-scroll" to="/contact#lgx-header">
                            Contact
						</Link>
                    </li>
                </ul>
            </div>
        );
    }
}
export default Navigation;