import React from 'react';
import { NavLink } from 'react-router-dom';

const Banner = props => {
	return (
		<section>
			<div className="lgx-banner lgx-banner-inner">
				<div className="lgx-page-inner">
					<div className="container">
						<div className="row">
							<div className="col-xs-12">
								<div className="lgx-heading-area">
									<div className="lgx-heading lgx-heading-white">
										<h2 className="heading">{props.call}</h2>
									</div>
									<ul className="breadcrumb">
										<li>
											<NavLink to="/">
												<i className="fa fa-home" aria-hidden="true" />
											</NavLink>
										</li>
										<li className="active">{props.event}</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};
export default Banner;
