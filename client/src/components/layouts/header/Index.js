import React, { Component } from 'react';
import Navigation from './Navigation';
import { NavLink } from 'react-router-dom';

class Index extends Component {
    render() {
        return (
            <header>
                <div id="lgx-header" className="lgx-header">
                    <div className="lgx-header-position lgx-header-position-white lgx-header-position-fixed ">
                        <div className="lgx-container">
                            <nav className="navbar navbar-default lgx-navbar">
                                <div className="navbar-header">
                                    <button
                                        type="button"
                                        className="navbar-toggle collapsed checkset"
                                        data-toggle="collapse"
                                        data-target="#navbar"
                                        aria-expanded="false"
                                        aria-controls="navbar"
                                    >
                                        <span className="sr-only">Toggle navigation</span>
                                        <span className="icon-bar" />
                                        <span className="icon-bar" />
                                        <span className="icon-bar" />
                                    </button>
                                    <div className="lgx-logo">
                                        <NavLink to="/" className="lgx-scroll">
                                            <img src="assets/img/logo.png" className="img-logo-header" alt="Eventhunt Logo" />
                                        </NavLink>
                                    </div>
                                </div>
                                <Navigation />
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}
export default Index;