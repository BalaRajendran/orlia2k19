import React, { Component } from 'react';
import TopSlide from './layouts/header/TopSlide';
import Rules from './layouts/Home/Rules';
import About from './layouts/Home/About';
import Promo from './layouts/Home/Promo';
import Coordinator from './layouts/Home/Coordinator';
import Info from './layouts/Home/Info'
import axios from 'axios';
var $ = require('jquery');

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			rcount: '',
			ccount: '',
		};
	}
	componentWillMount() {
		$(".checkset").addClass("collapsed");
		$("#navbar").addClass("collapsed");
		$("#navbar").removeClass("in");
		$(".checkset").attr("aria-expanded", "false");
		$("#navbar").attr("aria-expanded", "false");
		var self = this;
		axios.get('/backend/regcount').then(function (res) {
			self.setState({
				rcount:
					res.data['result'] === 'number'
						? parseInt(res.data['result'])
						: res.data['result'],
				ccount:
					res.data['result1'] === 'number'
						? parseInt(res.data['result1'])
						: res.data['result1'],
				visitors: res.data['visitors'] === 'number'
					? parseInt(res.data['visitors'])
					: res.data['visitors']
			});
		});
	}
	render() {
		return (
			<div>
				<TopSlide />
				<About />
				<Rules />
				<Info pcount={this.state.ccount} tcount={this.state.rcount} visitors={this.state.visitors}/>
				<Coordinator />
			</div>
		);
	}
}
export default App;
