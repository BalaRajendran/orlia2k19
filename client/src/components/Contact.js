import React, { Component } from 'react';
import Banner from './layouts/header/Banner';
import Feedback from './layouts/Footer/Feedback';
var $ = require('jquery');

class Contact extends Component {
	componentWillMount() {
		$(".checkset").addClass("collapsed");
		$("#navbar").addClass("collapsed");
		$("#navbar").removeClass("in");
		$(".checkset").attr("aria-expanded", "false");
		$("#navbar").attr("aria-expanded", "false");
	}
	render() {
		return (
			<div>
				<Banner call="GET IN TOUCH" event="CONTACT" />
				<Feedback />
			</div>
		);
	}
}
export default Contact;
