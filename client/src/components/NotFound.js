import React, { Component } from 'react';
import Banner from './layouts/header/Banner';
import { NavLink } from 'react-router-dom';

class NotFound extends Component {
	render() {
		return (
			<div>
				<Banner call="GET IN TOUCH" event="NOTFOUND" />
			<main>
				<div className="lgx-page-wrapper">
						<section>
							<div className="container">
								<div className="row justify-content-center">
									<div className="col-md-12 text-center">
										<img className="img-fluid" src="assets/img/404.png" alt="" />
										<h1>404</h1>
										<h2>Oops... Page Not Found!</h2>
										<p>
											Try using the button below to go to main page of the site{' '}
										</p><br/>
										<NavLink
											style={{color:"white"}}
											className="lgx-btn lgx-btn-red lgx-scroll"
											to="/#lgx-home"
										>
											<span>GO TO HOME</span>
										</NavLink>
									</div>
								</div>
							</div>
						</section>
				</div>
				</main>
			</div>
		);
	}
}
export default NotFound;
