import React, { Component } from 'react';
import Banner from './layouts/header/Banner';
import Registration from './layouts/Registration';
var $ = require('jquery');

class Register extends Component {
	componentWillMount() {
		$(".checkset").addClass("collapsed");
		$("#navbar").addClass("collapsed");
		$("#navbar").removeClass("in");
		$(".checkset").attr("aria-expanded", "false");
		$("#navbar").attr("aria-expanded", "false");
	}
	render() {
		return (
			<div>
				<Banner call="GET IN TOUCH" event="REGISTRATION" />
				<Registration />
			</div>

		);
	}
}
export default Register;
