var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Feedback = new Schema({
	name: String,
	phone: String,
	rollno: String,
	message:String,
	dateSent: String,
});

module.exports = mongoose.model('feedback', Feedback);
