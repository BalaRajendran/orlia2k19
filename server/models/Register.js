var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Register = new Schema({
	reg_id: String,
	teamname: String,
	eventname: String,
	count: String,
	dateSent: String,
});

module.exports = mongoose.model('register', Register);
