var express = require('express');
var router = express.Router();
const uuidv1 = require('uuid/v1');
const Register = require('../models/Register');
const Feedback = require('../models/Feedback');
const RegisterUser = require('../models/Reguser');
const Visitor = require('../models/Visitor');

router.route('/backend/regcount').get(function(req, res, next) {
	Register.find({}, { __v: 0 }).then(users =>
		RegisterUser.find({}, { __v: 0 }).then(users1 =>
			Visitor.find({}, { __v: 0 }).then(users3 =>
				res.status(200).json({
					result: users.length,
					result1: users1.length,
					visitors: users3[0].count,
				})
			)
		)
	);
});
router.route('/backend/visitors').get(function(req, res, next) {
	const date = new Date().toLocaleString('en-US', {
		timeZone: 'Asia/Calcutta',
	});
	Visitor.findOneAndUpdate(
		{},
		{
			$inc: { count: 1, date: date },
		}
	).exec();
});
router.route('/backend/feedback').post(function(req, res, next) {
	const { name, rollno, phone, message } = req.body;
	const date = new Date().toLocaleString('en-US', {
		timeZone: 'Asia/Calcutta',
	});
	const feedback = new Feedback({
		name,
		rollno,
		phone,
		message,
		date,
	});
	try {
		feedback
			.save()
			.then(feedback => {
				res.send('saved');
			})
			.catch(err => {
				res.send('Something Wrong! Try Again later');
			});
	} catch (err) {
		console.log('something wrong');
	}
});
router.route('/backend/postdata').post(function(req, res, next) {
	const { teamname, event_name, inputcount, name, roll } = req.body;
	const date = new Date().toLocaleString('en-US', {
		timeZone: 'Asia/Calcutta',
	});
	const random = uuidv1();
	const user = new Register({
		reg_id: random,
		teamname: teamname,
		eventname: event_name,
		count: inputcount,
		dateSent: date,
	});
	const find = async () => {
		global.k = 0;
		global.error = '';
		for (let i = 0; i < inputcount; i++) {
			var obj1 = roll['roll' + i];
			const item = await RegisterUser.find({
				eventname: event_name,
				rollno: obj1,
			});
			if (item.length != 0) {
				global.k = 1;
				global.error = 'Student Name already exist in this event';
			}
		}
		if (global.k == 0) {
			const item1 = await Register.find({
				teamname: teamname,
				eventname: event_name,
			});
			console.log(item1.length);
			if (item1.length != 0) {
				global.k = 1;
				global.error = 'Team name already exist in this event';
			}
		}
		if (global.k == 0) {
			user.save().then(data => {
				for (let i = 0; i < req.body.inputcount; i++) {
					var obj = req.body.name['name' + i];
					var obj1 = req.body.roll['roll' + i];
					var dept = obj1.slice(3, 5).toLowerCase();
					var user1 = new RegisterUser({
						eventname: event_name,
						reg_id: random,
						name: obj,
						dept: dept.toLowerCase(),
						eventname: event_name,
						rollno: obj1,
					});
					user1.save();
				}
			});
		}
		if (global.k != 0) {
			res.send(global.error);
		} else {
			res.send('saved');
		}
	};
	find();
});
module.exports = router;
