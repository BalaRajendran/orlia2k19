var express = require('express');
var http = require('http');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var keys = require('./config/keys');
var mongoose = require('mongoose');
var routes = require('./server/routes/index.js');
var app = express();
mongoose.Promise = require('bluebird');
mongoose.connect(
	keys.mongoURI,
	{ useNewUrlParser: true },
	function (err, db) {
		if (!err) {
			console.log('Database Connected!');
		} else {
			console.log('Database Not Connected!');
		}
	}
);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Credentials', true);
	res.header('Access-Control-Allow-Methods', '*');  // enables all the methods to take place
	return next();
});
app.use('/', routes);
module.exports = app;

if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/public/'));
	const path = require('path');
	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'client', 'public', 'index.html'));
	});
}
const PORT = process.env.PORT || 5000;
var listener = http.createServer(app);
var listener = app.listen(PORT, function () {
	console.log('Server Started on port ' + listener.address().port);
});
